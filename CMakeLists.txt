cmake_minimum_required(VERSION 3.5)

project(sftppp LANGUAGES CXX)

# Sometimes we can use just `find_package(libssh REQUIRED)` but this may not always work, depending
# on whether the dev package contains a CMake module for this. Pkg-Config seems to work everywhere.
find_package(PkgConfig REQUIRED)
pkg_check_modules(LibSSH QUIET IMPORTED_TARGET libssh)
if (NOT LibSSH_FOUND)
    message(FATAL_ERROR "Pkg-config could not find libssh.")
endif()
set_target_properties(PkgConfig::LibSSH PROPERTIES IMPORTED_GLOBAL TRUE)


set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Werror")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -DNDEBUG")

if (CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -fstandalone-debug")
endif()

string(APPEND CMAKE_SHARED_LINKER_FLAGS " -Wl,--no-undefined")


add_library(sftppp SHARED
    SftpClient.cpp
    SftpClient.h
)
target_link_libraries(
    sftppp
    PUBLIC PkgConfig::LibSSH
)

add_executable(sftppp_demo main.cpp)
target_link_libraries(sftppp_demo sftppp)


add_executable(sftppp_copy copy.cpp)
target_link_libraries(sftppp_copy sftppp)
