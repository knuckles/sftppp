# SFTP++

This is a small C++ wrapper around [libssh](https://libssh.org/) simplifying working with SFTP 
protocol (file transfer over SSH-2).
