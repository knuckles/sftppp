#include <fcntl.h>
#include <sys/stat.h>

#include <iostream>

#define SSH_NO_CPP_EXCEPTIONS
#include <libssh/libsshpp.hpp>
#include <libssh/sftp.h>
#include "SftpClient.h"

int main() {
    auto session = std::make_shared<ssh::Session>();
    sftppp::SshException::check(
        session->setOption(SSH_OPTIONS_LOG_VERBOSITY, SSH_LOG_WARNING),
        session->getCSession(), __PRETTY_FUNCTION__
    );
    sftppp::SshException::check(
        session->setOption(SSH_OPTIONS_HOST, "127.0.0.1"),
        session->getCSession(), __PRETTY_FUNCTION__
    );
    sftppp::SshException::check(
        session->setOption(SSH_OPTIONS_PORT, 2222), session->getCSession(), __PRETTY_FUNCTION__
    );
    sftppp::SshException::check(
        session->setOption(SSH_OPTIONS_USER, "foo"), session->getCSession(), __PRETTY_FUNCTION__
    );
    sftppp::SshException::check(
        session->connect(), session->getCSession(), __PRETTY_FUNCTION__
    );

    {
        // sftppp::PrivateKey authKey;
        // authKey.loadFromFile("a", "b");
        // sftppp::SshException::check(session->userauthPublickey(authKey.CKey()), session.get());

        const auto authResult = session->userauthPassword("pass");
        switch (authResult) {
            case SSH_AUTH_SUCCESS:
                break;
            case SSH_AUTH_PARTIAL:
            case SSH_AUTH_DENIED:
                throw std::runtime_error("Authentication failed.");
        }
    }

    sftppp::Client client(session);

    const std::string dirPath = "/";
    auto rootDir = client.openDirectory(dirPath);
    const auto& dirList = rootDir->list();
    std::cout << dirPath << " has " << dirList.size() << " items:\n";
    for (const auto& entry : dirList) {
        std::cout << +entry->type << "\t" << entry->name << "\n";
    }
    std::cout << std::endl;

    {
        std::cout << "Testing file reads\n";

        auto testFile = client.openFile("/upload/testr", O_RDONLY, S_IRWXU);
        std::cout << "Test file contents:\n" << testFile.get() << std::endl;
    }

    {
        std::cout << "Testing file writes\n";

        auto testFile = client.openFile(
            "/upload/testw",
            O_RDWR | O_CREAT | O_TRUNC,
            S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH
        );
        std::ostream os(testFile.get());
        for (int i = 1; i <= 10; i++) {
            os << "File line " << i << "\n";
        }
        os.flush();
        std::cout << "Wrote 10 lines.\n";
        std::cout << "OS pos " << os.tellp() << "\n";

        testFile->pubseekoff(12, std::ios_base::beg, std::ios_base::in);
        std::cout << "Skipped 1 line:\n\n" << testFile.get() << std::endl;

        for (int i = 1; i <= 2; i++) {
            os << "File line " << i << "\n";
        }
        os.flush();
        std::cout << "Wrote 2 more lines.\n";
        std::cout << "OS pos " << os.tellp() << "\n";

        testFile->pubseekoff(121, std::ios_base::beg, std::ios_base::in);
        std::cout << "Skipped 10 lines:\n\n" << testFile.get() << std::endl;

        os.seekp(-24, std::ios_base::end);
        for (int i = 1; i <= 2; i++) {
            os << "File line " << 20 + i << "\n";
        }
        os.flush();
        std::cout << "Overwrote 2 lines.\n";
        std::cout << "OS pos " << os.tellp() << "\n";

        testFile->pubseekoff(-26, std::ios_base::end, std::ios_base::in);
        std::cout << "Last 2 lines:\n\n" << testFile.get() << std::endl;
    }

    return 0;
}
