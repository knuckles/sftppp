#include "SftpClient.h"

#include <cstring>
#include <fcntl.h>
#include <sstream>

#define SSH_NO_CPP_EXCEPTIONS
#include <libssh/libsshpp.hpp>
#include <libssh/sftp.h>


#define FNAME_STRING std::string{__PRETTY_FUNCTION__}

namespace sftppp {

namespace {

ScopedAttributes ownAttrs(
    sftp_attributes attrs, const sftp_session session, const std::string& where
) {
    if (!attrs)
        throw SftpException(session, where);
    return ScopedAttributes(attrs, sftp_attributes_free);
}

inline const char* charPtrFromString(const std::string& s) {
    return s.empty() ? nullptr : s.c_str();
}

std::string formatSftpError(const sftp_session session) {
    const int sftpError = sftp_get_error(session);

    std::stringstream ss;
    ss << "SFTP error (" << sftpError << "): ";

    switch(sftpError) {
        case SSH_FX_OK:
            ss << "No error";
            break;
        case SSH_FX_EOF:
            ss << "End-of-file encountered";
            break;
        case SSH_FX_NO_SUCH_FILE:
            ss << "File doesn't exist";
            break;
        case SSH_FX_PERMISSION_DENIED:
            ss << "Permission denied";
            break;
        case SSH_FX_FAILURE:
            ss << "Generic failure";
            break;
        case SSH_FX_BAD_MESSAGE:
            ss << "Garbage received from server";
            break;
        case SSH_FX_NO_CONNECTION:
            ss << "No connection has been set up";
            break;
        case SSH_FX_CONNECTION_LOST:
            ss << "Connection lost";
            break;
        case SSH_FX_OP_UNSUPPORTED:
            ss << "Operation not supported by the server";
            break;
        case SSH_FX_INVALID_HANDLE:
            ss << "Invalid file handle";
            break;
        case SSH_FX_NO_SUCH_PATH:
            ss << "No such file or directory path exists";
            break;
        case SSH_FX_FILE_ALREADY_EXISTS:
            ss << "File or directory already exists";
            break;
        case SSH_FX_WRITE_PROTECT:
            ss << "Filesystem is write-protected";
            break;
        case SSH_FX_NO_MEDIA:
            ss << "No media in remote drive";
            break;
        default:
            ss << "Unknown error";
    }
    ss << '.';

    const int sshError = ssh_get_error_code(session->session);
    if (sshError != SSH_OK) {
        ss << " SSH error (" << sshError << "): " << ssh_get_error(session->session) << '.';
    }

    return ss.str();
}

}  // namespace


// SshException

// static
void SshException::check(int retCode, const ssh_session session, const std::string& where) {
    if (retCode < 0)
        throw SshException(formatMessage(session, where));
}

// static
std::string SshException::formatMessage(const ssh_session session, const std::string& where) {
    std::stringstream ss;
    ss << "SSH error " << ssh_get_error_code(session)
       << " in `" << where << "`: "
       << ssh_get_error(session);
    return ss.str();
}


// SftpException
SftpException::SftpException(const sftp_session session, const std::string& where)
    : std::runtime_error(formatMessage(session, where)) {
}

// static
void SftpException::check(const int retCode, const sftp_session session, const std::string& where) {
    if (retCode == 0)
        return;
    throw SftpException(formatMessage(session, where));
}

// static
std::string SftpException::formatMessage(const sftp_session session, const std::string& where) {
    return formatSftpError(session) + " In: " + where + ".";
}

// Key

PrivateKey::PrivateKey()
: _handle(ssh_key_new()) {}

PrivateKey::PrivateKey(ssh_key handle)
: _handle(handle) {}

PrivateKey::~PrivateKey() {
    ssh_key_free(_handle);
}

void PrivateKey::decodeBase64(const std::string& keyData, const std::string& password) {
    if (ssh_pki_import_privkey_base64(
            charPtrFromString(keyData),
            charPtrFromString(password),
            nullptr, nullptr,
            &_handle
        ) != SSH_OK)
        throw SshException("Could not load key data");
}

void PrivateKey::loadFromFile(const std::string& keyFilePath, const std::string& password) {
    if (ssh_pki_import_privkey_file(
            charPtrFromString(keyFilePath),
            charPtrFromString(password),
            nullptr, nullptr,
            &_handle
        ) != SSH_OK)
        throw SshException("Could not load key data");
}


// SftpFile

File::File(
    const std::ios_base::openmode mode,
    const sftp_file handle,
    const size_t bufSize,
    const size_t putBackSize
)
: _mode(mode),
  _handle(handle),
  _putBackSize(putBackSize),
  _readBuf((_mode & std::ios_base::in) ? std::max(bufSize, putBackSize) + putBackSize : 0),
  _writeBuf((_mode & std::ios_base::out) ? bufSize : 0) {
    if (!_handle)
        throw SshException("No file handle provided.");

    _remoteReadPos = _remoteWritePos = sftp_tell64(_handle);
    resetInputArea();
    resetOutputArea();
}

File::~File() {
    if (_mode & std::ios_base::out)
        sync();
    sftp_close(_handle);
}

void File::setBlocking(bool blocking) {
    if (blocking)
        sftp_file_set_blocking(_handle);
    else
        sftp_file_set_nonblocking(_handle);
}

ScopedAttributes File::getAttributes() const {
    return ownAttrs(sftp_fstat(_handle), _handle->sftp, FNAME_STRING);
}

int File::underflow() {
    if (!(_mode & std::ios_base::in))
        return traits_type::eof();

    if (gptr() < egptr())
        return traits_type::to_int_type(*gptr());

    char* const base = &_readBuf.front();
    char* start = base;

    if (eback() == base) {  // Not the first read; allow "putting back".
        std::memmove(base, egptr() - _putBackSize, _putBackSize);
        start += _putBackSize;
    }
    const auto cap = _readBuf.size() - (start - base);

    SftpException::check(sftp_seek64(_handle, _remoteReadPos), _handle->sftp, FNAME_STRING);
    const auto fetched = sftp_read(_handle, start, cap);

    if (fetched == 0)
        return traits_type::eof();

    // NOTE: sftp_read retuns unsigned type. Comparison without a cast would always yield false.
    if (static_cast<int64_t>(fetched) < 0) {
        // TODO: Maybe it's best to not throw (even though allowed by the standard)?
        throw std::ios_base::failure(
            ssh_get_error(_handle->sftp->session),
            std::make_error_code(std::errc::io_error)  // TODO: Review this. Pass SSH/SFTP error codes.
        );
    }

    const auto tellResult = sftp_tell64(_handle);
    // NOTE: sftp_tell64 retuns unsigned type. Comparison without a cast would always yield false.
    if (static_cast<int64_t>(tellResult) < 0)
        throw SftpException(_handle->sftp, FNAME_STRING);
    _remoteReadPos = tellResult;
    setg(base, start, start + fetched);

    return traits_type::to_int_type(*gptr());
}

int File::overflow(int c) {
    if (!(_mode & std::ios_base::out))
        return traits_type::eof();

    if ((c != traits_type::eof()) && (pptr() < epptr())) {
        *pptr() = c;
        return c;
    }

    const auto syncResult = sync();
    if (syncResult != 0)
        return traits_type::eof();

    if (c != traits_type::eof()) {
        *this->pptr() = traits_type::to_char_type(c);
        pbump(1);
    }

    return c;
}

int File::sync() {
    const auto buffered = pptr() - pbase();
    if (buffered > 0) {
        const auto seekResult = sftp_seek64(_handle, _remoteWritePos);
        if (seekResult < 0)
            return -1;

        const auto written = sftpWrite(pbase(), buffered);
        if (written < 0)
            return -1;

        // TODO: Handle `if (written < buffered)` and update input area to keep unwritten data.
        // This situation seems to be practically impossible from the implementation of v-0.7.

        const uint64_t tellResult = sftp_tell64(_handle);
        // NOTE: sftp_tell64 retuns unsigned type. Comparison without a cast would always
        // yield false.
        if (static_cast<int64_t>(tellResult) < 0)
            throw SftpException(_handle->sftp, FNAME_STRING);
        const off_type nextWritePos = tellResult;
        const off_type firstReadPos = _remoteReadPos - (egptr() - eback());
        // off type is a platform dependent type. It could be signed and unsigned numeric type.
        const off_type lastReadPos = _remoteReadPos ? _remoteReadPos - 1 : _remoteReadPos;
        const bool invalidateReadBuffer =
            (_remoteWritePos <= firstReadPos && firstReadPos < nextWritePos) ||
            (_remoteWritePos <= lastReadPos && lastReadPos < nextWritePos);

        resetOutputArea();
        _remoteWritePos = nextWritePos;

        // Invalidate read buffer if underlying file content could be altered by the write.
        if (invalidateReadBuffer) {
            resetInputArea();
        }
    }

    if (fileSyncExtSupported()) {
        if (sftp_fsync(_handle) < 0)
            return -1;
    }

    return 0;
}

File::pos_type File::seekpos(const pos_type newPos, const std::ios_base::openmode which) {
    if (which & std::ios_base::in) {
        auto offset = newPos - pos_type(_remoteReadPos);
        auto newGetPtr = gptr() + offset;
        if (eback() <= newGetPtr && newGetPtr < egptr()) {
            // Simply shift current read pointer inside the current buffer if it contains the
            // content at the desired file position.
            setg(eback(), newGetPtr, egptr());
        }
        else {
            // Invalidate the input buffer to cause `underflow` and, as a result, fetch the block of
            // data at desired position inside the file.
            _remoteReadPos = newPos;
            resetInputArea();
        }
    }

    if (which & std::ios_base::out) {
        if (_remoteWritePos != newPos) {
            sync();  // Write out whatever we may have.
            _remoteWritePos = newPos;
        }
    }

    return newPos;
}

File::pos_type File::seekoff (
    const File::off_type off, const std::ios_base::seekdir way, const std::ios_base::openmode which
) {
    if (way == std::ios_base::cur
        && (which & std::ios_base::in)
        && (which & std::ios_base::out)
    ) { // Ambigous combination of arguments - fail.
        return pos_type(off_type(-1));
    }

    off_type newPos = 0;

    switch (way) {
        case std::ios_base::cur: {
            const auto currPos = (which & std::ios_base::in) ? _remoteReadPos : _remoteWritePos;
            newPos = currPos + off;
            break;
        }
        case std::ios_base::end: {
            const auto attrs = getAttributes();
            newPos = attrs->size + off;
            break;
        }
        case std::ios_base::beg: {
            newPos = off;
            break;
        }
        default:  // Should never happen.
            return -1;
    }

    return seekpos(newPos, which);
}

void File::resetInputArea() {
    char* const end = &_readBuf.front() + _readBuf.size();
    setg(end, end, end);
}

void File::resetOutputArea() {
    setp(&_writeBuf.front(), &_writeBuf.front() + _writeBuf.size());
}

bool File::fileSyncExtSupported() {
    return sftp_extension_supported(_handle->sftp, "fsync@openssh.com", "1");
}

ssize_t File::sftpWrite(const void *buf, size_t count) {
    const char* ptr = static_cast<const char*>(buf);
    size_t remainder = count;
    while (remainder) {
        const ssize_t written = sftp_write(_handle, ptr, std::min(remainder, _chunkSize ));
        // To prevent an endless loop we treet 0 as en error.
        if (written <= 0)
            return -1;
        remainder -= written;
        ptr += written;
    }
    return count;
}


// Directory

Directory::Directory(sftp_dir handle)
: _handle(handle) {
    if (!_handle)
        throw SshException("No directory handle provided.");

    while (auto attrs = sftp_readdir(_handle->sftp, _handle)) {
        _contents.push_back(ownAttrs(attrs, _handle->sftp, FNAME_STRING));
    }
    if (!sftp_dir_eof(_handle))  // Failed to read all of directory contents.
        throw SftpException(_handle->sftp, FNAME_STRING);
}

Directory::~Directory() {
    sftp_closedir(_handle);
}


// SftpClient

Client::Client(std::shared_ptr<ssh::Session> sshSession)
    : _sshSession(sshSession) {
    _sftpSession = sftp_new(_sshSession->getCSession());
    if (!_sftpSession) {
        throw SftpException("Could not create SFTP session.");  // TODO: Check for SSH error.
    }
    SftpException::check(sftp_init(_sftpSession), _sftpSession, FNAME_STRING);
}

Client::~Client() {
    sftp_free(_sftpSession);
}

std::optional<std::string> Client::lastError() const {
    const auto sftpErrCode = sftp_get_error(_sftpSession);
    if (sftpErrCode == SSH_FX_OK)
        return std::nullopt;
    return formatSftpError(_sftpSession);
}

ScopedAttributes Client::getAttributes(const std::string& path) const {
    return ownAttrs(
        sftp_stat(_sftpSession, charPtrFromString(path)),
        _sftpSession,
        FNAME_STRING
    );
}

ScopedAttributes Client::getLinkAttributes(const std::string& path) const {
    return ownAttrs(
        sftp_lstat(_sftpSession, charPtrFromString(path)),
        _sftpSession,
        FNAME_STRING
    );
}

void Client::setAttributes(const std::string& path, sftp_attributes attrs) {
    SftpException::check(
        sftp_setstat(_sftpSession, charPtrFromString(path), attrs),
        _sftpSession,
        FNAME_STRING
    );
}

std::unique_ptr<Directory> Client::openDirectory(const std::string& path) const {
    const auto handle = sftp_opendir(_sftpSession, charPtrFromString(path));
    if (!handle)
        throw SftpException(_sftpSession, FNAME_STRING);
    return std::make_unique<Directory>(handle);
}

std::unique_ptr<File> Client::openFile(
    const std::string& path,
    int flags,
    mode_t mode,
    const size_t bufSize,
    const size_t putBackSize
) {
    const auto handle = sftp_open(_sftpSession, charPtrFromString(path), flags, mode);
    if (!handle)
        throw SftpException(_sftpSession, FNAME_STRING);

    const auto stdOpenMode =
        (flags & O_RDWR) ? (std::ios_base::in | std::ios_base::out) :
        (flags & O_WRONLY) ? std::ios_base::out :
        std::ios_base::in;

    return std::make_unique<File>(stdOpenMode, handle, bufSize, putBackSize);
}

void Client::rename(const std::string& srcPath, const std::string& destPath) {
    SftpException::check(
        sftp_rename(_sftpSession, charPtrFromString(srcPath), charPtrFromString(destPath)),
        _sftpSession,
        FNAME_STRING
    );
}

void Client::removeFile(const std::string& path) {
    SftpException::check(
        sftp_unlink(_sftpSession, charPtrFromString(path)),
        _sftpSession,
        FNAME_STRING
    );
}

void Client::removeDirectory(const std::string& path) {
    SftpException::check(
        sftp_rmdir(_sftpSession, charPtrFromString(path)),
        _sftpSession,
        FNAME_STRING
    );
}

void Client::createDirectory(const std::string& path, const mode_t mode) {
    SftpException::check(
        sftp_mkdir(_sftpSession, charPtrFromString(path), mode),
        _sftpSession,
        FNAME_STRING
    );
}

} // namespace sftppp
