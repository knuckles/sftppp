#include <fcntl.h>
#include <sys/stat.h>

#include <iostream>
#include <fstream>
#include <iterator>

#define SSH_NO_CPP_EXCEPTIONS
#include <libssh/libsshpp.hpp>
#include <libssh/sftp.h>
#include "SftpClient.h"

int main(int argc, char* argv[]) {
    if (argc != 7) {
        std::cout
            << "Usage:\n"
            << "\t" << argv[0]
            << " <host> <port> <user> <key-path> "
            << "<local-file-path> <remote-file-path>\n";
        return EXIT_FAILURE;
    }
    const char* host = argv[1];
    const char* port = argv[2];
    const char* user = argv[3];
    const char* kpath = argv[4];
    const char* lpath = argv[5];
    const char* rpath = argv[6];
    
    auto session = std::make_shared<ssh::Session>();
    sftppp::SshException::check(
        session->setOption(SSH_OPTIONS_LOG_VERBOSITY, SSH_LOG_WARNING),
        session->getCSession(),
        __PRETTY_FUNCTION__
    );
    sftppp::SshException::check(
        session->setOption(SSH_OPTIONS_HOST, host),
        session->getCSession(), __PRETTY_FUNCTION__
    );
    sftppp::SshException::check(
        session->setOption(SSH_OPTIONS_PORT, std::atoi(port)),
        session->getCSession(),
        __PRETTY_FUNCTION__
    );
    sftppp::SshException::check(
        session->setOption(SSH_OPTIONS_USER, user),
        session->getCSession(),
        __PRETTY_FUNCTION__
    );

    sftppp::SshException::check(
        session->connect(), session->getCSession(), __PRETTY_FUNCTION__
    );

    sftppp::PrivateKey authKey;
    authKey.loadFromFile(kpath, "");
    sftppp::SshException::check(
        session->userauthPublickey(authKey.CKey()),
        session->getCSession(),
        __PRETTY_FUNCTION__);

    const auto authResult = session->userauthPassword("");
    switch (authResult) {
        case SSH_AUTH_SUCCESS:
            break;
        case SSH_AUTH_PARTIAL:
        case SSH_AUTH_DENIED:
            throw std::runtime_error("Authentication failed.");
    }

    sftppp::Client client(session);
    auto testFile = client.openFile(rpath,
            O_WRONLY | O_CREAT | O_TRUNC,
            S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP,
            2 * 1024 * 1024,
            0
    );
    std::ostream os(testFile.get());
    os.exceptions(std::ios_base::failbit);
    std::ifstream source(lpath, std::ios::binary);
    source.unsetf(std::ios::skipws);

    std::copy(
        std::istream_iterator<char>(source),
        std::istream_iterator<char>(),
        std::ostream_iterator<char>(os)
    );

    os.flush();
    os.exceptions(std::ios_base::goodbit);

    return EXIT_SUCCESS;
}
