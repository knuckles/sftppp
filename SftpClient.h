#pragma once

#include <functional>
#include <memory>
#include <optional>
#include <streambuf>
#include <string>
#include <vector>

#include <sys/stat.h>


struct ssh_key_struct;
struct ssh_session_struct;
struct sftp_dir_struct;
struct sftp_file_struct;
struct sftp_session_struct;
struct sftp_attributes_struct;

typedef struct ssh_key_struct* ssh_key;
typedef struct ssh_session_struct* ssh_session;
typedef struct sftp_dir_struct* sftp_dir;
typedef struct sftp_file_struct* sftp_file;
typedef struct sftp_session_struct* sftp_session;
typedef struct sftp_attributes_struct* sftp_attributes;


namespace ssh {
    class Session;
}

namespace sftppp {

// Exception class provided by libssh is not ideal: it doesn't inherit from std::exception and
// copies strings too much. This is an alternative to it.
class SshException : public std::runtime_error {
public:
    using std::runtime_error::runtime_error;

    // Check libssh return code and raise an exception in case of an error.
    static void check(int retCode, const ssh_session session, const std::string& where);

private:
    static std::string formatMessage(const ssh_session session, const std::string& where);
};


class SftpException : public std::runtime_error {
public:
    using std::runtime_error::runtime_error;

    explicit SftpException(const sftp_session session, const std::string& where);

    static void check(const int retCode, const sftp_session session, const std::string& where);

private:
    static std::string formatMessage(const sftp_session session, const std::string& where);
};


class PrivateKey {
public:
    PrivateKey();
    PrivateKey(const PrivateKey&) = delete;
    const PrivateKey& operator=(const PrivateKey&) = delete;
    explicit PrivateKey(ssh_key handle);
    ~PrivateKey();

    ssh_key CKey() const { return _handle; }

    void decodeBase64(const std::string& keyData, const std::string& password);
    void loadFromFile(const std::string& keyFilePath, const std::string& password);

private:
    ssh_key _handle = nullptr;
};


using ScopedAttributes = std::unique_ptr<
    sftp_attributes_struct,
    std::function<void(sftp_attributes)>
>;


class File : public std::streambuf {
public:
    explicit File(
        const std::ios_base::openmode mode,
        const sftp_file handle,
        const size_t bufSize,
        const size_t putBackSize
    );
    File(const File&) = delete;
    File& operator=(const File&) = delete;
    ~File() override;

    void setBlocking(bool blocking);

    // NOTE: libssh leaves the `name` member null!
    ScopedAttributes getAttributes() const;

protected:
    // std::streambuf:
    int underflow() override;
    int overflow(int c) override;  // Allowed to throw.
    int sync() override;

    pos_type seekpos(const pos_type pos, const std::ios_base::openmode which) override;
    pos_type seekoff(
        const off_type off, const std::ios_base::seekdir way, const std::ios_base::openmode which
    ) override;

private:
    void resetInputArea();
    void resetOutputArea();
    bool fileSyncExtSupported();

    ssize_t sftpWrite(const void *buf, size_t count);

    std::ios_base::openmode _mode;
    sftp_file _handle;

    const size_t _putBackSize;
    std::vector<char> _readBuf;
    std::vector<char> _writeBuf;

    off_type _remoteReadPos = 0;  // File position to read next buffer from.
    off_type _remoteWritePos = 0;  // File position to write current buffer to.

    // SSH restricts the max package size by 32k.
    // Sftp also must follow the limitation.
    // (https://tools.ietf.org/html/rfc4253#section-6.1).
    static constexpr size_t _chunkSize = 32 * 1024;
};


class Directory {
public:
    explicit Directory(sftp_dir handle);
    Directory(const Directory&) = delete;
    Directory& operator=(const Directory&) = delete;
    ~Directory();

    const std::vector<ScopedAttributes>& list() const { return _contents; }

private:
    sftp_dir _handle;
    std::vector<ScopedAttributes> _contents;
};


class Client {
public:
    explicit Client(std::shared_ptr<ssh::Session> sshSession);
    Client(const Client&) = delete;
    Client& operator=(const Client&) = delete;
    ~Client();

    /// Since std streams are not great at error reporting, this method serves to get access to a
    /// human readable description of the last occured error, if any.
    std::optional<std::string> lastError() const;

    ScopedAttributes getAttributes(const std::string& path) const;
    ScopedAttributes getLinkAttributes(const std::string& path) const;
    void setAttributes(const std::string& path, sftp_attributes attrs);

    std::unique_ptr<Directory> openDirectory(const std::string& path) const;

    std::unique_ptr<File> openFile(
        const std::string& path,
        int flags,
        mode_t mode,
        const size_t bufSize = 16 * 1024,
        const size_t putBackSize = 8
    );

    void rename(const std::string& srcPath, const std::string& destPath);
    void removeFile(const std::string& path);
    void removeDirectory(const std::string& path);
    void createDirectory(const std::string& path, const mode_t mode);

private:
    std::shared_ptr<ssh::Session> _sshSession;
    sftp_session _sftpSession;
};

}  // namespace sftppp
